
from fresnel import *
from TransferMatrixMethod import *
import os
import matplotlib.pyplot as plt


def load_gold():
    with open(os.path.join("materials", "au_jc.txt")) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0]*1000)
        index.append(data[1] + 1j*data[2])
    return np.array(wavelength), np.array(index)


def load_tinitride():
    with open(os.path.join("materials", "peald_tin_onsi.txt")) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j*data[2])
    return np.array(wavelength), np.array(index)


if __name__ == "__main__":
    base_wavelengths, base_gold_index = load_gold()
    wavelengths = np.linspace(600, 900, num=10)
    gold_index = list()
    for w in wavelengths:
        gold_index.append(np.interp(w, base_wavelengths, base_gold_index))
    gold_index = np.array(gold_index)
    layer_1_index = np.array([1.485])
    layer_3_index = np.array([1.485])
    layer_1_thickness = np.array([1000])
    layer_2_thickness = np.array([10])
    layer_3_thickness = np.array([1000])

    layer_0 = Layer(np.array([1000]), np.array([1.7]), wavelengths)
    layer_1 = Layer(layer_1_thickness, layer_1_index, wavelengths)
    layer_2 = Layer(layer_2_thickness, gold_index, wavelengths)
    layer_3 = Layer(layer_3_thickness, layer_3_index, wavelengths)

    angle_deg = np.linspace(20, 80, num=1000)
    angle = angle_deg * np.pi/180

    layer_0 = Layer(np.array([1000]), np.array([1.485]), wavelengths)
    test = MMatrix(wavelengths, angle)
    test.layers = [layer_0, layer_1, layer_2, layer_3]

    refs = test.amplitude_reflection_p()
    refs = np.square(np.abs(refs))
    # indices = [i for i, r in enumerate(wavelengths)]
    # for r in np.swapaxes(refs, 0, 0):
    #     plt.plot(angle_deg, r)
    # plt.legend()
    coeffs = test
    thicknesses = [10, 20, 30, 40, 50]
    for thickness in thicknesses:
        layer_2.thickness = np.array([thickness])
        test.layers = [layer_0, layer_1, layer_2, layer_3]
        r = np.square(np.abs(test.amplitude_reflection_p()))
        plt.plot(angle_deg, r[0], label="Thickness = {} nm".format(thickness))

    plt.legend()
    plt.show()
