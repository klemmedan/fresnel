import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import os
from TransferMatrixMethod import MMatrix, Layer

t0 = 5.0
w0 = 785.0
th0 = 61.49
tio2_n = 2.6142

default_path = os.path.join("materials", "peald_tin_slowrecipe_onsi.txt")


class SingleLayerWidget:

    def __init__(self):
        self.plotting_angle = True
        self.metal_thickness = t0
        self.metal_path = default_path
        self.wavelengths = np.linspace(785, 785, 1)
        self.fig, self.ax = plt.subplots(figsize=(10, 6))
        self.angle_deg = np.linspace(20, 89, num=4000)
        self.angle_rad = self.angle_deg * np.pi / 180
        self.r = self.update_r()
        self.lines = list()

        self.init_plot()
        self.thickness_slider = self.init_thickness_slider()
        self.x_slider = self.init_x_slider()
        self.radio = self.init_angle_wavelength_radio_buttons()

        plt.show()

    def update_r(self):
        transfer = MMatrix(self.wavelengths, self.angle_rad)
        transfer.layers = self.generate_layers()
        r = transfer.power_reflection_p()
        if not self.plotting_angle:
            r = np.transpose(r)
        return r

    def init_plot(self):
        self.r = self.update_r()

        l1, = plt.plot(self.angle_deg, self.r[0], lw=2)
        self.lines = [l1]
        plt.axis([min(self.angle_deg) - 5, max(self.angle_deg) + 5, 0, 1.1])
        plt.ylabel("Reflection")
        plt.xlabel("Angle (deg)")
        plt.subplots_adjust(left=0.45, bottom=0.35)

    def init_thickness_slider(self):
        axcolor = 'lightgoldenrodyellow'
        axthickness = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
        slider = Slider(axthickness, 'Metal Thickness', 1, 100.0, valinit=t0)
        slider.on_changed(self.update_from_thickness_slider)
        return slider

    def init_angle_wavelength_radio_buttons(self):
        axcolor = 'lightgoldenrodyellow'
        rax = plt.axes([0.05, 0.7, 0.15, 0.15], facecolor=axcolor)
        radio = RadioButtons(rax, ('angle', 'wavelength'))
        radio.on_clicked(self.update_on_angle_wavelength_radio)
        return radio

    def init_x_slider(self):
        axcolor = 'lightgoldenrodyellow'
        axthickness = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
        if self.plotting_angle:
            slider = Slider(axthickness, 'Wavelength', 400.0, 1500.0,
                            valinit=w0)
        else:
            slider = Slider(axthickness, 'Theta', 55, 65, valinit=th0)

        slider.on_changed(self.update_from_x_slider)
        return slider

    def generate_layers(self):
        data_w, data_index = import_file_data(self.metal_path)
        metal_index = list()
        for w in self.wavelengths:
            metal_index.append(np.interp(w, data_w, data_index))
        metal_index = np.array(metal_index)

        layer_1_index = np.array([1.34])
        layer_3_index = np.array([1.33])
        layer_1_thickness = np.array([3000])
        layer_2_thickness = np.array([self.metal_thickness])
        layer_3_thickness = np.array([10000])

        layer_tio2_thickness = np.array([1.1])
        layer_tio2_index = np.array([tio2_n])

        layer_0 = Layer(np.array([10000]), np.array([1.52]), self.wavelengths)
        layer_1 = Layer(layer_1_thickness, layer_1_index, self.wavelengths)
        layer_tio2_bottom = Layer(layer_tio2_thickness, layer_tio2_index, self.wavelengths)
        layer_2 = Layer(layer_2_thickness, metal_index, self.wavelengths)
        layer_tio2_top = Layer(layer_tio2_thickness, layer_tio2_index, self.wavelengths)
        layer_3 = Layer(layer_3_thickness, layer_3_index, self.wavelengths)
        return [layer_0, layer_1, layer_2, layer_3]

    def update_from_thickness_slider(self, val):
        self.metal_thickness = self.thickness_slider.val
        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        self.fig.canvas.draw_idle()

    def update_from_x_slider(self, val):
        if self.plotting_angle:
            self.wavelengths = np.array([self.x_slider.val])
        else:
            self.angle_deg = np.array([self.x_slider.val])
            self.angle_rad = self.angle_deg * np.pi / 180

        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        self.fig.canvas.draw_idle()

    def update_on_angle_wavelength_radio(self, label):
        self.plotting_angle = not self.plotting_angle
        self.switch_angles_and_wavelengths()
        self.update_plot_on_radio_button_pressed()
        self.update_x_slider_on_radio_button_pressed()

    def update_x_slider_on_radio_button_pressed(self):
        # axcolor = 'lightgoldenrodyellow'
        # axthickness = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
        # slider = Slider(axthickness, 'Wavelength', 400.0, 1500.0,
        #                 valinit=self.wavelengths[0])
        # slider.
        self.x_slider.ax.patch.set_visible(False)
        self.x_slider.ax.axis('off')
        self.x_slider.label.set_visible(False)
        self.x_slider.valtext.set_visible(False)
        self.x_slider.vline.set_visible(False)
        self.x_slider = self.init_x_slider()
        self.x_slider.ax.patch.set_visible(True)
        self.x_slider.ax.axis('on')

    def update_plot_on_radio_button_pressed(self):
        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        if self.plotting_angle:
            l.set_xdata(self.angle_deg)
            self.ax.set_xlim([0, 90])
            self.ax.set_xlabel("Angle (deg)")
        else:
            l.set_xdata(self.wavelengths)
            self.ax.set_xlim([550, 1050])
            self.ax.set_xlabel("Wavelength (nm)")
        self.fig.canvas.draw_idle()

    def switch_angles_and_wavelengths(self):
        if self.plotting_angle:  # Angle is the current x-axis
            self.wavelengths = np.linspace(w0, w0, 1)
            self.angle_deg = np.linspace(20, 80, num=4000)
            self.angle_rad = self.angle_deg * np.pi / 180
        else:
            self.wavelengths = np.linspace(400.0, 1500.0, num=4000)
            self.angle_deg = np.linspace(th0, th0, num=1)
            self.angle_rad = self.angle_deg * np.pi / 180


def import_file_data(path):
    with open(path) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j*data[2])
    return np.array(wavelength), np.array(index)


if __name__ == "__main__":
    SingleLayerWidget()
