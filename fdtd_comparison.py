from TransferMatrixMethod import *
import numpy as np
import matplotlib.pyplot as plt
import os


def import_file_data(path):
    with open(path) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j*data[2])
    return np.array(wavelength), np.array(index)


def import_fdtd(filename):
    path = os.path.join("fdtd_prism_results", filename)
    wavelengths = list()
    transmission = list()
    with open(path, 'r') as f:
        for line in f:
            data = line.split(", ")
            print(data)
            wavelengths.append(float(data[0])*1000)
            transmission.append(float(data[1]))
    return wavelengths, transmission


wavelengths = np.linspace(550, 1000, num=1000)
angle = np.array([78.01*np.pi/180])
layer_thickness = np.linspace(1, 100, num=100)
metal_path = os.path.join("materials", "peald_tin_onsi.txt")

data_w, data_index = import_file_data(metal_path)

metal_index = list()

for w in wavelengths:
    metal_index.append(np.interp(w, data_w, data_index))
metal_index = np.array(metal_index)

layer_1_index = np.array([1.485])
layer_3_index = np.array([1.485])
layer_1_thickness = np.array([1000])
layer_2_thickness = np.array([10])
layer_3_thickness = np.array([1000])

layer_0 = Layer(np.array([1000]), np.array([1.52]), wavelengths)
layer_1 = Layer(layer_1_thickness, layer_1_index, wavelengths)
layer_2 = Layer(layer_2_thickness, metal_index, wavelengths)
layer_3 = Layer(layer_3_thickness, layer_3_index, wavelengths)

tmatrix = MMatrix(wavelengths, angle)
tmatrix.layers = [layer_0, layer_1, layer_2, layer_3]
reflection = np.transpose(tmatrix.power_reflection_p())[0]

small_fdtd_w, small_fdtd_t = import_fdtd("prism_spectrum_785.txt")
big_fdtd_w, big_fdtd_t = import_fdtd("kretschmann_spectrum_prims_huge.txt")

plt.plot(wavelengths, reflection, label="analytic calculations")
plt.plot(small_fdtd_w, small_fdtd_t, label="fdtd smaller prism")
plt.plot(big_fdtd_w, big_fdtd_t, label="fdtd larger prism")
plt.legend()

plt.savefig(os.path.join("fdtd_prism_results", "comparison.png"))


print(np.interp(785, data_w, data_index))
plt.show()
