import os

with open(os.path.join("materials", "placeholder.txt"), 'r') as f:
    lines = f.readlines()

wavelength = list()
n = list()
k = list()

for line in lines:
    split_line = line.split()
    wavelength.append(float(split_line[0])*1000)
    n.append(split_line[1])
    k.append(split_line[2])

with open(os.path.join("materials", "fixed_ag_jc.txt"), 'w') as f:
    for w, index_n, index_k in zip(wavelength, n, k):
        f.write("{:.1f}\t{}\t{}\n".format(w, index_n, index_k))
