from TransferMatrixMethod import *
import matplotlib.pyplot as plt
import numpy as np
import os


def plot_angles(coefficients, angles, wavelengths=None):
    for i, coef in enumerate(coefficients):
        if wavelengths is not None:
            plt.plot(angles, coef, label="Wavelength = {} nm".format(wavelengths[i]))
        else:
            plt.plot(angles, coef)
    if wavelengths is not None:
        plt.legend()
    plt.show()


def plot_wavelengths(coefficients, wavelengths, angles=None):
    for i, coef in enumerate(coefficients):
        if angles is not None:
            plt.plot(wavelengths, coef, label="Angle = {}".format(angles[i]))
        else:
            plt.plot(wavelengths, coef)
    if angles is not None:
        plt.legend()
    plt.show()

def import_file_data(path):
    with open(path) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j * data[2])
    return np.array(wavelength), np.array(index)


def generate_layers(film_thickness=0.0):
    data_w, data_index = import_file_data(metal_path)
    metal_index = list()
    for w in wavelengths:
        metal_index.append(np.interp(w, data_w, data_index))
    metal_index = np.array(metal_index)

    layer_1_index = np.array([1.33])
    layer_3_index = np.array([1.33])
    layer_1_thickness = np.array([1000])
    layer_2_thickness = np.array([metal_thickness])
    layer_3_thickness = np.array([1000])

    film_layer_index = np.array([1.76])
    film_layer_thickness = np.array([film_thickness])

    layer_0 = Layer(np.array([1000]), np.array([1.52]), wavelengths)
    layer_1 = Layer(layer_1_thickness, layer_1_index, wavelengths)
    layer_2 = Layer(layer_2_thickness, metal_index, wavelengths)
    film_layer = Layer(film_layer_thickness, film_layer_index, wavelengths)
    layer_3 = Layer(layer_3_thickness, layer_3_index, wavelengths)
    return [layer_0, layer_1, layer_2, film_layer, layer_3]


if __name__ == "__main__":
    metal_path = os.path.join("materials", "peald_tin_slowrecipe_onsi.txt")
    metal_thickness = np.array([10.0])
    theta = np.array([61.06])
    theta_rad = theta * np.pi / 180
    wavelengths = np.linspace(600, 1200, num=5000)
    transfer = MMatrix(wavelengths, theta_rad)
    transfer.layers = generate_layers(0.0)
    r_zero = np.transpose(transfer.power_reflection_p())
    transfer.layers = generate_layers(5.0)
    r_five = np.transpose(transfer.power_reflection_p())
    transfer.layers = generate_layers(10.0)
    r_ten = np.transpose(transfer.power_reflection_p())

    plt.plot(wavelengths, r_zero[0], label="0 nm alumina")
    plt.plot(wavelengths, r_five[0], label="5 nm alumina")
    plt.plot(wavelengths, r_ten[0], label="10 nm alumina")
    plt.xlabel("Wavelength (nm)")
    plt.ylabel("Reflection")
    plt.legend()
    plt.savefig("tin_sensing.png", dpi=600)
    plt.show()