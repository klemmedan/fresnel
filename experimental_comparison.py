import os
import matplotlib.pyplot as plt
import numpy as np
from TransferMatrixMethod import *

def load_experiment_data(path):
    with open(path, 'r') as f:
        lines = f.readlines()
    wavs = list()
    ts = list()
    for line in lines:
        x = line.split()
        wavs.append(float(x[0]))
        ts.append(float(x[1]))

    return wavs, ts


def import_file_data(path):
    with open(path) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j*data[2])
    return np.array(wavelength), np.array(index)


experiment_path = os.path.join("experimental",
                               "initial_dip_tmte.txt")
exp_w, exp_t = load_experiment_data(experiment_path)
metal_index = list()
metal_path = os.path.join("materials", "peald_tin_onsi.txt")
wavelengths = np.linspace(400, 1100, num=1000)
angle = np.array([61.26*np.pi/180])
data_w, data_index = import_file_data(metal_path)

for w in wavelengths:
    metal_index.append(np.interp(w, data_w, data_index))
metal_index = np.array(metal_index)

layer_1_index = np.array([1.34])
layer_3_index = np.array([1.33])
layer_1_thickness = np.array([1000])
layer_2_thickness = np.array([7.55])
layer_3_thickness = np.array([1000])

layer_0 = Layer(np.array([1000]), np.array([1.52]), wavelengths)
layer_1 = Layer(layer_1_thickness, layer_1_index, wavelengths)
layer_2 = Layer(layer_2_thickness, metal_index, wavelengths)
layer_3 = Layer(layer_3_thickness, layer_3_index, wavelengths)

tmatrix = MMatrix(wavelengths, angle)
tmatrix.layers = [layer_0, layer_1, layer_2, layer_3]
reflection = np.transpose(tmatrix.power_reflection_p())[0]
print(reflection)

plt.plot(exp_w, [t/200 for t in exp_t], label="experiment")
plt.plot(wavelengths, reflection, label="theory")

plt.ylim([0, 1])
plt.xlim([550, 900])
plt.legend()
plt.xlabel("Wavelength (nm)")
plt.ylabel("Reflection")
plt.savefig(os.path.join("experimental", "cytop_exp.png"),
            dpi=600)
plt.show()