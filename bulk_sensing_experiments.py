import os
import matplotlib.pyplot as plt


experimental_folder = "experimental"
bulk_sensing_folder = "2018-05-25_bulksensing"
data_files = ["spectrum_dip_0p_concentration.txt", "spectrum_dip_1p_concentration.txt", "spectrum_dip_2p_concentration.txt"]
data_paths = list()

for file in data_files:
    data_paths.append(os.path.join(experimental_folder, bulk_sensing_folder,
                                   file))

w = list()
r = list()
with open(data_paths[0], 'r') as f:
    lines = f.readlines()
    for line in lines:
        split_line = line.split()
        w.append(float(split_line[0]))
        r.append(float(split_line[1]))
plt.plot(w, r, label="0% Concentration")
w.clear()
r.clear()

with open(data_paths[1], 'r') as f:
    lines = f.readlines()
    for line in lines:
        split_line = line.split()
        w.append(float(split_line[0]))
        r.append(float(split_line[1]))
plt.plot(w, r, label="1% Concentration")
w.clear()
r.clear()

with open(data_paths[2], 'r') as f:
    lines = f.readlines()
    for line in lines:
        split_line = line.split()
        w.append(float(split_line[0]))
        r.append(float(split_line[1]))
plt.plot(w, r, label="2% Concentration")

plt.legend()
plt.xlim([500, 900])
plt.ylim([20, 130])
plt.ylabel("Reflectance (%)")
plt.xlabel("Wavelength (nm)")
plt.savefig(os.path.join(experimental_folder, bulk_sensing_folder, "sensing_plot.png"), dpi=600)
plt.show()