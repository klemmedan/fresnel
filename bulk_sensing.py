import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import os
from TransferMatrixMethod import MMatrix, Layer

# Bulk index parameters
bind_min = 1.33
bind_max = 1.35
bind_def = 1.33


# Metal thickness parameters
t0 = 10.0
tmin = 0.0
tmax = 25.0

ft0 = 1.0

w0 = 785.0
wmin = 400
wmax = 1500

# Good PEALD Theta values for glass 1.52, cytop 1.33
# th0 = 61.07
# thmin = 60  # This is for slider only
# thmax = 62  # This is for slider only

# Good PEALD Theta values for glass 1.51, cytop 1.34
th0 = 62.1
thmin = 61  # This is for slider only
thmax = 63  # This is for slider only

# Good Gold Theta Values
# th0 = 62.9
# thmin = 50  # This is for slider only
# thmax = 85  # This is for slider only


cytop_t = 1200
cytop_ind = 1.31


plt_thmin = 20  # This is for the plot xlim
plt_thmax = 80  # This is for the plot xlim

plt_wmin = 400
plt_wmax = 1000

# default_path = os.path.join("materials", "peald_tin_onsi.txt")
default_path = os.path.join("materials", "peald_tin_onsi.txt")
# default_path = os.path.join("materials", "peald_tin_slowrecipe_onsi.txt")
# default_path = os.path.join("materials", "au_jc.txt")


class SingleLayerWidget:

    def __init__(self):
        self.plotting_angle = True
        self.metal_thickness = t0
        self.film_thickness = ft0
        self.metal_path = default_path
        self.wavelengths = np.linspace(w0, w0, 1)
        self.bulk_index = bind_def
        self.fig, self.ax = plt.subplots(figsize=(12, 8))
        self.angle_deg = np.linspace(plt_thmin, plt_thmax, num=10000)
        self.angle_rad = self.angle_deg * np.pi / 180
        self.r = self.update_r()
        self.lines = list()

        self.init_plot()
        self.thickness_slider = self.init_thickness_slider()
        self.bulk_thickness_slider = self.init_sensing_film_thickness_slider()
        self.x_slider = self.init_x_slider()
        self.radio = self.init_angle_wavelength_radio_buttons()

        plt.show()

    def update_r(self):
        transfer = MMatrix(self.wavelengths, self.angle_rad)
        transfer.layers = self.generate_layers()
        r = transfer.power_reflection_p()
        if not self.plotting_angle:
            r = np.transpose(r)
        return r

    def init_plot(self):
        self.r = self.update_r()
        l1, = plt.plot(self.angle_deg, self.r[0], lw=2)
        self.lines = [l1]
        plt.axis([min(self.angle_deg) - 5, max(self.angle_deg) + 5, 0, 1.1])
        plt.ylabel("Reflection")
        plt.xlabel("Angle (deg)")
        plt.subplots_adjust(left=0.25, bottom=0.25)

    def init_thickness_slider(self):
        axcolor = 'lightgoldenrodyellow'
        axthickness = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
        slider = Slider(axthickness, 'Metal Thickness', tmin, tmax, valinit=t0)
        slider.on_changed(self.update_from_thickness_slider)
        return slider

    def init_angle_wavelength_radio_buttons(self):
        axcolor = 'lightgoldenrodyellow'
        rax = plt.axes([0.05, 0.7, 0.15, 0.15], facecolor=axcolor)
        radio = RadioButtons(rax, ('angle', 'wavelength'))
        radio.on_clicked(self.update_on_angle_wavelength_radio)
        return radio

    def init_sensing_film_thickness_slider(self):
        axcolor = 'lightgoldenrodyellow'
        axthickness = plt.axes([0.25, 0.05, 0.65, 0.03], facecolor=axcolor)
        slider = Slider(axthickness, 'Bulk Index', bind_min, bind_max, valinit=bind_def)
        slider.on_changed(self.update_from_film_slider)
        return slider

    def init_x_slider(self):
        axcolor = 'lightgoldenrodyellow'
        axthickness = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
        if self.plotting_angle:
            slider = Slider(axthickness, 'Wavelength', wmin, wmax,
                            valinit=w0)
        else:
            slider = Slider(axthickness, 'Theta', thmin, thmax, valinit=th0)

        slider.on_changed(self.update_from_x_slider)
        return slider

    def generate_layers(self):
        data_w, data_index = import_file_data(self.metal_path)
        metal_index = list()
        for w in self.wavelengths:
            metal_index.append(np.interp(w, data_w, data_index))
        metal_index = np.array(metal_index)

        layer_1_index = np.array([cytop_ind])
        layer_3_index = np.array([self.bulk_index])
        layer_1_thickness = np.array([cytop_t])
        layer_2_thickness = np.array([self.metal_thickness])
        layer_3_thickness = np.array([1000])

        film_layer_index = np.array([1.76])
        film_layer_thickness = np.array([self.film_thickness])

        layer_0 = Layer(np.array([1000]), np.array([1.51]), self.wavelengths)
        layer_1 = Layer(layer_1_thickness, layer_1_index, self.wavelengths)
        layer_2 = Layer(layer_2_thickness, metal_index, self.wavelengths)
        film_layer = Layer(film_layer_thickness, film_layer_index, self.wavelengths)
        layer_3 = Layer(layer_3_thickness, layer_3_index, self.wavelengths)
        return [layer_0, layer_1, layer_2, film_layer, layer_3]

    def update_from_thickness_slider(self, val):
        self.metal_thickness = self.thickness_slider.val
        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        self.fig.canvas.draw_idle()

    def update_from_film_slider(self, val):
        self.bulk_index = self.bulk_thickness_slider.val
        print(self.bulk_index)
        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        self.fig.canvas.draw_idle()

    def update_from_x_slider(self, val):
        if self.plotting_angle:
            self.wavelengths = np.array([self.x_slider.val])
        else:
            self.angle_deg = np.array([self.x_slider.val])
            self.angle_rad = self.angle_deg * np.pi / 180

        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        self.fig.canvas.draw_idle()

    def update_on_angle_wavelength_radio(self, label):
        self.plotting_angle = not self.plotting_angle
        self.switch_angles_and_wavelengths()
        self.update_plot_on_radio_button_pressed()
        self.update_x_slider_on_radio_button_pressed()

    def update_x_slider_on_radio_button_pressed(self):
        self.x_slider.ax.patch.set_visible(False)
        self.x_slider.ax.axis('off')
        self.x_slider.label.set_visible(False)
        self.x_slider.valtext.set_visible(False)
        self.x_slider.vline.set_visible(False)
        self.x_slider = self.init_x_slider()
        self.x_slider.ax.patch.set_visible(True)
        self.x_slider.ax.axis('on')

    def update_plot_on_radio_button_pressed(self):
        self.r = self.update_r()
        for ref, l in zip(self.r, self.lines):
            l.set_ydata(ref)
        if self.plotting_angle:
            l.set_xdata(self.angle_deg)
            self.ax.set_xlim([0, 90])
            self.ax.set_xlabel("Angle (deg)")
        else:
            l.set_xdata(self.wavelengths)
            self.ax.set_xlim([plt_wmin, plt_wmax])
            self.ax.set_xlabel("Wavelength (nm)")
        self.fig.canvas.draw_idle()

    def switch_angles_and_wavelengths(self):
        if self.plotting_angle:  # Angle is the current x-axis
            self.wavelengths = np.linspace(w0, w0, 1)
            self.angle_deg = np.linspace(20, 80, num=4000)
            self.angle_rad = self.angle_deg * np.pi / 180
        else:
            self.wavelengths = np.linspace(400.0, 1500.0, num=4000)
            self.angle_deg = np.linspace(th0, th0, num=1)
            self.angle_rad = self.angle_deg * np.pi / 180



def import_file_data(path):
    with open(path) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j*data[2])
    return np.array(wavelength), np.array(index)


if __name__ == "__main__":
    SingleLayerWidget()
