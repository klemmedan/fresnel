import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import os
from TransferMatrixMethod import MMatrix, Layer

# Bulk index parameters
bind_min = 1.33
bind_max = 1.35
bind_def = 1.33


# Metal thickness parameters
t0 = 10.0
tmin = 0.0
tmax = 25.0

ft0 = 1.0

w0 = 785.0
wmin = 400
wmax = 1500

# Good PEALD Theta values for glass 1.52, cytop 1.33
# th0 = 61.07
# thmin = 60  # This is for slider only
# thmax = 62  # This is for slider only

# Good PEALD Theta values for glass 1.51, cytop 1.34
th0 = 62.1
thmin = 61  # This is for slider only
thmax = 63  # This is for slider only

# Good Gold Theta Values
# th0 = 62.9
# thmin = 50  # This is for slider only
# thmax = 85  # This is for slider only


cytop_t = 1200
cytop_ind = 1.34


plt_thmin = 20  # This is for the plot xlim
plt_thmax = 80  # This is for the plot xlim

plt_wmin = 400
plt_wmax = 1000

# default_path = os.path.join("materials", "peald_tin_onsi.txt")
default_path = os.path.join("materials", "peald_tin_onsi.txt")
# default_path = os.path.join("materials", "peald_tin_slowrecipe_onsi.txt")
# default_path = os.path.join("materials", "au_jc.txt")


def generate_layers(sensing_index, tin_thickness, wavelengths):
    data_w, data_index = import_file_data(default_path)
    metal_index = list()
    for w in wavelengths:
        metal_index.append(np.interp(w, data_w, data_index))
    metal_index = np.array(metal_index)

    layer_1_index = np.array([cytop_ind])
    layer_3_index = np.array([sensing_index])
    layer_1_thickness = np.array([cytop_t])
    layer_2_thickness = np.array([tin_thickness])
    layer_3_thickness = np.array([1000])

    layer_0 = Layer(np.array([1000]), np.array([1.51]), wavelengths)
    layer_1 = Layer(layer_1_thickness, layer_1_index, wavelengths)
    layer_2 = Layer(layer_2_thickness, metal_index, wavelengths)
    layer_3 = Layer(layer_3_thickness, layer_3_index, wavelengths)
    return [layer_0, layer_1, layer_2, layer_3]


def calculate_r(sensing_index, tin_thickness, wavelengths, angles):
    transfer = MMatrix(wavelengths, angles)
    transfer.layers = generate_layers(sensing_index, tin_thickness, wavelengths=wavelengths)
    r = transfer.power_reflection_p()

    # Transpose r if plotting wavelength on x axis instead of angle.
    r = np.transpose(r)
    return r


def import_file_data(path):
    with open(path) as f:
        lines = f.readlines()

    wavelength = list()
    index = list()
    for line in lines:
        data = [float(x) for x in line.split()]
        wavelength.append(data[0])
        index.append(data[1] + 1j*data[2])
    # plt.plot(wavelength, np.real(index))
    # plt.plot(wavelength, np.imag(index))
    # plt.show()
    return np.array(wavelength), np.array(index)


def get_dip_position(r, wavelengths):
    # mylist = list(r[0])
    index = np.argmin(r[0], axis=0)
    return wavelengths[index]


def calculate_sensitivity_curve(tin_thickness, angle, wavelengths):
    dip_positions = list()
    delta_index = list()
    base_wavelength = 0
    for index in np.linspace(1.33, 1.3325, 20):
        r = calculate_r(index, tin_thickness, wavelengths, angle)
        dip_position = get_dip_position(r, wavelengths)
        if index == 1.33:
            base_wavelength = dip_position
        dip_positions.append(dip_position - base_wavelength)
        delta_index.append(index - 1.33)
    return delta_index, dip_positions


def save_sensitivity(delta_index, dip_positions):
    with open("sensitivity_tin_thickness{0}.txt".format(tin_thickness), 'w') as f:
        for di, dp in zip(delta_index, dip_positions):
            f.write("{0} {1}\n".format(di, dp))


def plot_tin_angle(tin_thickness):
    angles = np.array([th*2.0*np.pi/180 for th in np.linspace(58.75, 59.1, 500)])

    for w in range(500, 860, 20):
        wavelengths = np.array([w])
        r = calculate_r(1.33, tin_thickness, wavelengths, angles)
        r = r.transpose()
        plt.plot(180*angles/(2*np.pi), r[0], label="{0} nm".format(w))
    plt.legend()
    plt.show()


def plot_sensitivity_curves():
    for tin_t in range(4, 20, 2):
        delta_index, dip_positions = load_sensitivity_curve(tin_t)
        plt.plot(delta_index, dip_positions, label="TiN thickness = {0} nm".format(tin_t))
    plt.legend()
    plt.show()

def load_sensitivity_curve(tin_thickness):

    with open("sensitivity_cytop1200_tin_thickness_{0}.txt".format(tin_thickness), 'r') as f:
        lines = f.readlines()

    delta_indices = list()
    dip_positions = list()
    for line in lines:
        di, dp = line.split()
        delta_indices.append(float(di))
        dip_positions.append(float(dp))
    return delta_indices, dip_positions

if __name__ == "__main__":
    angles = np.array([th*2.0*np.pi/180 for th in np.linspace(58.9, 59.1, 500)])


    # angle for tin thickness = 20:
    angles = np.array([58.72])
    # angle for tin thickness = 18:
    # angles = np.array([58.810])
    # angle for tin thickness = 16:
    # angles = np.array([58.850])
    # angle for tin thickness = 14:
    # angles = np.array([58.900])
    # angle for tin thickness = 12:
    # angles = np.array([58.925])
    # angle for tin thickness = 10:
    # angles = np.array([58.97])
    # angle for tin thickness = 8:
    # angles = np.array([59.000])
    # angle for tin thickness = 6:
    # angles = np.array([59.03])
    # angle for tin thickness = 4:
    angles = np.array([58.97])

    angles = 2*np.pi*angles/180
    wavelengths = np.linspace(400, 1500, 400)

    tin_thickness = 4
    # plot_tin_angle(tin_thickness)
    delta_index, dip_positions = calculate_sensitivity_curve(tin_thickness, angles, wavelengths)
    save_sensitivity(delta_index, dip_positions)
    plt.plot(delta_index , dip_positions)
    plt.show()
    # plot_sensitivity_curves()