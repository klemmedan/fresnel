import matplotlib.pyplot as plt
import numpy as np
import math


class SingleInterfaceFresnel:

    def __init__(self):
        self.n_incident = np.array([1.0])
        self.n_transmission = np.array([1.2])
        self.theta_incident = np.array([90])
        self._calc_ni = np.array([])
        self._calc_nt = np.array([])
        self._calc_thetai = np.array([])

    def theta_transmitted(self):
        """
        Calculate the transmitted angle of incidence based on snells law.
        :return: 2D array of transmitted angles with first axis being
        different indices (wavelength dependent) and second axis being
        incident angles.
        :rtype: np.array
        """
        self._shape_arrays()
        fraction = self._calc_ni * self._calc_nt

        return np.arcsin(fraction * np.sin(self._calc_thetai) + 0.0j)

    def amplitude_reflection_p(self):
        theta_t = self.theta_transmitted()  # 2D Matrix of transmissions

        numerator = self._calc_nt*np.cos(self._calc_thetai) - self._calc_ni*np.cos(theta_t)
        denominator = self._calc_nt*np.cos(self._calc_thetai) + self._calc_ni*np.cos(theta_t)
        return numerator/denominator

    def amplitude_reflection_s(self):
        theta_t = self.theta_transmitted()

        numerator = self._calc_ni*np.cos(self._calc_thetai) - \
                    self._calc_nt*np.cos(theta_t)
        denominator = np.outer(self._calc_ni, np.cos(self._calc_thetai)) + \
                      self._calc_nt * np.cos(theta_t)
        return numerator/denominator

    def amplitude_transmission_p(self):
        theta_t = self.theta_transmitted()

        numerator = 2*self._calc_ni*np.cos(self._calc_thetai)
        denominator = self._calc_nt*np.cos(self._calc_thetai) + \
                      self._calc_ni* np.cos(theta_t)
        return numerator/denominator

    def amplitude_transmission_s(self):
        return self.amplitude_reflection_s() + 1

    def power_reflection_s(self):
        return np.square(np.abs(self.amplitude_reflection_s()))

    def power_reflection_p(self):
        return np.square(np.abs(self.amplitude_reflection_p()))

    def power_transmission_s(self):
        """
        Return the s-polarization transmission power matrix
        :return: transmission power matrix
        :rtype: np.array
        """
        return 1 - self.power_reflection_s()

    def power_transmission_p(self):
        return 1 - self.power_reflection_p()

    def _shape_indices(self):
        if self.n_incident.size > self.n_transmission.size:
            self.n_transmission = np.repeat(self.n_transmission[0],
                                            self.n_incident.size)
        elif self.n_incident.size < self.n_transmission.size:
            self.n_incident = np.repeat(self.n_incident[0],
                                        self.n_transmission.size)

    def _shape_arrays(self):
        num_wavelengths = max([self.n_incident.size, self.n_transmission.size])
        num_angles = self._calculate_num_angles()
        theta_shape = self.theta_incident.shape
        self._shape_indices()

        if len(theta_shape) == 2:  # Theta's already dispersed.
            self._calc_thetai = self.theta_incident
        else:
            self._calc_thetai = self.theta_incident[np.newaxis, :].repeat(
                num_wavelengths, 0)
        self._calc_ni = np.swapaxes(self.n_incident[np.newaxis, :].repeat(num_angles, 0), 0, 1)
        self._calc_nt = np.swapaxes(self.n_transmission[np.newaxis, :].repeat(num_angles, 0), 0, 1)

    def _calculate_num_angles(self):
        angle_shape = self.theta_incident.shape
        num_angles = 0
        if len(angle_shape) == 1:
            num_angles = self.theta_incident.size
        elif len(angle_shape) == 2:
            num_angles = len(self.theta_incident[0])
        return num_angles



if __name__ == "__main__":
    fresnel = SingleInterfaceFresnel()
    theta_deg = np.linspace(10, 20, num=2)
    theta_rad = theta_deg*np.pi/180
    fresnel.theta_incident = theta_rad
    fresnel.n_transmission = np.linspace(1, 2, num=5)

    # plt.plot(fresnel.n_transmission, np.real(fresnel.amplitude_reflection_s()).transpose()[0])
    # plt.plot(fresnel.n_transmission, np.imag(fresnel.amplitude_reflection_s()).transpose()[0])
    # plt.plot(theta_deg, fresnel.amplitude_reflection_p()[0])
    # plt.plot(theta_deg, fresnel.amplitude_transmission_s()[0])
    # plt.plot(theta_deg, fresnel.amplitude_transmission_p()[0])
    # plt.plot(fresnel.n_transmission, fresnel.power_reflection_s().transpose()[0])
    # plt.plot(fresnel.n_transmission, fresnel.power_transmission_s().transpose()[0])

    # plt.plot(theta_deg, fresnel.power_reflection_p()[0])
    # plt.plot(theta_deg, fresnel.power_reflection_s()[0])
    # plt.plot(theta_deg, fresnel.power_transmission_p()[0])
    # plt.plot(theta_deg, fresnel.power_transmission_s()[0])
    # plt.show()
    print(fresnel.theta_transmitted())
    fresnel.theta_incident = fresnel.theta_transmitted()
    print(fresnel.theta_incident - fresnel.theta_transmitted())
    fresnel._shape_arrays()
    print(fresnel._calc_thetai)
    print(fresnel._calc_nt)
    print(fresnel._calc_ni)
    # print(theta_rad[np.newaxis, :].repeat(4, 0))
    # print(np.swapaxes(theta_rad[np.newaxis, :].repeat(4, 0), 0, 1))
    # print(theta_rad)