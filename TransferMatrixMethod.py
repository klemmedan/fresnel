from fresnel import SingleInterfaceFresnel
import numpy as np
import matplotlib.pyplot as plt


class MMatrix:

    def __init__(self, wavelengths, initial_angles):
        """
        Method to calculate the M-Matrices for stack of layers.
        :param wavelengths: wavelengths that will be used.
        :param initial_angles: Initial input angles
        """
        self.layers = list()
        self.wavelengths = np.array(wavelengths)
        self.angles = np.array(initial_angles)

        self._matrices = list()
        self._calc_angles = self._init_calc_angles()
        self._calc_wavelengths = self._init_calc_wavelengths()

    def amplitude_reflection_p(self):
        self._fix_layer_indices()
        self.generate_matrices_p()
        m_matrix = self._multiply_matrices()
        r = -m_matrix[1][0]/m_matrix[1][1]
        return r

    def amplitude_transmission_p(self):
        self._fix_layer_indices()
        self.generate_matrices_p()
        m_matrix = self._multiply_matrices()
        t = m_matrix[0][0] - m_matrix[0][1]*m_matrix[1][0]/m_matrix[1][1]
        return t

    def amplitude_reflection_s(self):
        self._fix_layer_indices()
        self.generate_matrices_s()
        m_matrix = self._multiply_matrices()
        r = -m_matrix[1][0]/m_matrix[1][1]
        return r

    def amplitude_transmission_s(self):
        self._fix_layer_indices()
        self.generate_matrices_s()
        m_matrix = self._multiply_matrices()
        t = m_matrix[0][0] - m_matrix[0][1]*m_matrix[1][0]/m_matrix[1][1]
        return t

    def power_reflection_p(self):
        r = self.amplitude_reflection_p()
        return np.square(np.abs(r))

    def power_transmission_p(self):
        t = self.amplitude_transmission_p()
        n1 = self.layers[0].ref_index
        n2 = self.layers[-1].ref_index
        cos_1 = self._layer_angle_cos(self.layers[0])
        cos_2 = self._layer_angle_cos(self.layers[-1])
        return np.square(np.abs(t))*(n2*cos_2)/(n1*cos_1)

    def power_reflection_s(self):
        r = self.amplitude_reflection_s()
        return np.square(np.abs(r))

    def power_transmission_s(self):
        t = self.amplitude_transmission_s()
        n1 = self.layers[0].ref_index
        n2 = self.layers[-1].ref_index
        cos_1 = self._layer_angle_cos(self.layers[0])
        cos_2 = self._layer_angle_cos(self.layers[-1])
        return np.square(np.abs(t))*(n2*cos_2)/(n1*cos_1)

    def _multiply_matrices(self):
        return_mat = None
        for matrix in reversed(self._matrices):
            if return_mat is None:
                return_mat = matrix
            else:
                return_mat = np.matmul(return_mat, matrix)
        return_mat = np.swapaxes(return_mat, 0, 2)
        return_mat = np.swapaxes(return_mat, 1, 3)
        return return_mat

    def generate_matrices_s(self):
        # Iterate through the layers and generate appropriate propagation and
        # interface matrices.
        self._matrices = list()  # Clear any previous matrices
        for i, layer in enumerate(self.layers[1:-1]):
            interface_matrix = self._generate_interface_matrix_s(self.layers[
                                                                     i], layer)
            self._matrices.append(interface_matrix)
            propagation_matrix = self._generate_propagation_matrix(layer)
            self._matrices.append(propagation_matrix)
        # Now generate the final interface matrix
        im = self._generate_interface_matrix_s(self.layers[-2], self.layers[-1])
        self._matrices.append(im)

    def generate_matrices_p(self):
        # Iterate through the layers and generate appropriate propagation and
        # interface matrices.
        self._matrices = list()  # Clear any previous matrices
        for i, layer in enumerate(self.layers[1:-1]):
            interface_matrix = self._generate_interface_matrix_p(self.layers[i], layer)
            self._matrices.append(interface_matrix)
            propagation_matrix = self._generate_propagation_matrix(layer)
            self._matrices.append(propagation_matrix)
        # Now generate the final interface matrix
        im = self._generate_interface_matrix_p(self.layers[-2], self.layers[-1])
        self._matrices.append(im)

    def _generate_propagation_matrix(self, layer):
        index = layer.ref_index
        k = 2*np.pi/self._calc_wavelengths
        d = layer.thickness
        cos = self._layer_angle_cos(layer)
        phi = index*k*d*cos[np.newaxis, :]
        zeroes = np.zeros(phi.shape)

        # phi_top = 1j * np.real(phi) - np.abs(np.imag(phi))
        # phi_bot = -1j * np.real(phi) - np.abs(np.imag(phi))

        top_row = np.concatenate([np.power(np.e, 1j*phi), zeroes], axis=0)[
                  np.newaxis, :]
        bot_row = np.concatenate([zeroes, np.power(np.e, -1j*phi)], axis=0)[
            np.newaxis, :]
        matrix = np.concatenate([top_row, bot_row], axis=0)
        matrix = matrix.swapaxes(0, 2)
        matrix = matrix.swapaxes(1, 3)
        return matrix

    def _generate_interface_matrix_p(self, layer_1, layer_2):
        """
        Generate the M-Matrix for the interface between two layers
        :param layer_1: Left-hand side layer that light propagates from
        :type layer_1: Layer
        :param layer_2: Right-hand side layer that light propagates into
        :type layer_2: Layer
        :return: M-Matrix for the interface of these two layers.
        :rtype: numpy.core.ndarray
        """
        index_1 = layer_1.ref_index
        index_2 = layer_2.ref_index
        cos_1 = self._layer_angle_cos(layer_1)
        cos_2 = self._layer_angle_cos(layer_2)
        r12 = (index_2*cos_1 - index_1*cos_2) / (index_1*cos_2 + index_2*cos_1)
        r21 = (index_1*cos_2 - index_2*cos_1) / (index_2*cos_1 + index_1*cos_2)
        t12 = (index_1/index_2)*(r12 + 1)[np.newaxis, :]
        t21 = (index_2/index_1)*(r21 + 1)[np.newaxis, :]
        r12 = r12[np.newaxis, :]
        r21 = r21[np.newaxis, :]
        top_row = np.concatenate([t12 - (r12 * r21)/t21, r21/t21], axis=0)[
                  np.newaxis, :]
        bot_row = np.concatenate([-r12/t21, 1/t21], axis=0)[np.newaxis, :]
        matrix = np.concatenate([top_row, bot_row], axis=0)
        matrix = matrix.swapaxes(0, 2)
        matrix = matrix.swapaxes(1, 3)
        return matrix

    def _generate_interface_matrix_s(self, layer_1, layer_2):
        """
        Generate the M-Matrix for the interface between two layers
        :param layer_1: Left-hand side layer that light propagates from
        :type layer_1: Layer
        :param layer_2: Right-hand side layer that light propagates into
        :type layer_2: Layer
        :return: M-Matrix for the interface of these two layers.
        :rtype: numpy.core.ndarray
        """
        index_1 = layer_1.ref_index
        index_2 = layer_2.ref_index
        cos_1 = self._layer_angle_cos(layer_1)
        cos_2 = self._layer_angle_cos(layer_2)
        r12 = (index_1*cos_1 - index_2*cos_2) / (index_1*cos_1 + index_2*cos_2)
        r21 = (index_2*cos_2 - index_1*cos_1) / (index_2*cos_2 + index_1*cos_1)
        t12 = (r12 + 1)[np.newaxis, :]
        t21 = (r21 + 1)[np.newaxis, :]
        r12 = r12[np.newaxis, :]
        r21 = r21[np.newaxis, :]
        top_row = np.concatenate([t12 - (r12 * r21)/t21, r21/t21], axis=0)[
                  np.newaxis, :]
        bot_row = np.concatenate([-r12/t21, 1/t21], axis=0)[np.newaxis, :]
        matrix = np.concatenate([top_row, bot_row], axis=0)
        matrix = matrix.swapaxes(0, 2)
        matrix = matrix.swapaxes(1, 3)
        return matrix

    def _layer_angle_cos(self, current_layer):
        """
        Generate the cos of the angles in the current layer.
        :param current_layer: Layer that we are currently in
        :type current_layer: Layer
        :return:
        """
        original_layer = self.layers[0]
        return np.sqrt(1 - np.square((original_layer.ref_index/current_layer.ref_index)*np.sin(self._calc_angles)) + 0.0j)

    def _layer_angle_sin(self, current_layer):
        """
        Generate the sin of the angles in the current layer.
        :param current_layer: Layer that we are currently in
        :type current_layer: Layer
        :return:
        """
        original_layer = self.layers[0]
        return (original_layer.ref_index/current_layer.ref_index)*np.sin(self._calc_angles)

    def _init_calc_angles(self):
        """
        :return: 2D matrix of angles for calculating more easily.
        :rtype: numpy.core.ndarray
        """
        num_wavelengths = self.wavelengths.size
        return self.angles[np.newaxis, :].repeat(num_wavelengths, 0)

    def _init_calc_wavelengths(self):
        """
        :return: 2D matrix of wavelengths for calculating more easily.
        :rtype: numpy.core.ndarray
        """
        num_angles = self.angles.size
        return np.swapaxes(self.wavelengths[np.newaxis, :].repeat(num_angles,
                                                                  0), 0, 1)

    def _fix_layer_indices(self):
        num_wavelengths = self.wavelengths.size
        num_angles = self.angles.size
        for layer in self.layers:
            index = layer.ref_index
            if index.size == 1:
                index = np.repeat(index[0], num_wavelengths)
            if len(index.shape) < 2:
                index = np.swapaxes(index[np.newaxis, :].repeat(num_angles,
                                                                0), 0, 1)
            layer.ref_index = index


class Layer:

    def __init__(self, thickness, index, wavelength):
        """
        Class representing a single optical layer.
        :param thickness: Thickness of the layer
        :type thickness: numpy.core.ndarray
        :param index: Refractive index of the layer
        :type index: numpy.core.ndarray
        :param wavelength: Wavelength of the incoming light
        :type wavelength: numpy.core.ndarray
        """
        self.thickness = thickness
        self.ref_index = index
        self.wavelength = wavelength
        self._calc_wavelength = None
        self._calc_index = None

    def propagation_matrix(self, theta):
        """
        Get the propogation matrix for light coming in at angles theta.
        :param theta: array of light angles in the layer.
        :type theta: numpy.core.ndarray
        :return: matrices for propogation at each angle.
        :rtype: numpy.core.ndarray
        """
        self._reshape_arrays()
        calc_theta = self._fix_theta(theta)
        self._fix_wavelength(calc_theta)
        # phase_w = self.wavelength.
        phase = (2*np.pi/self._calc_wavelength) * self._calc_index*self.thickness*np.cos(calc_theta)[np.newaxis, :]
        zeroes = np.zeros(phase.shape)
        top_row = np.concatenate([np.power(np.e, -1j*phase), zeroes], axis=0)[
                  np.newaxis, :]
        bot_row = np.concatenate([zeroes, np.power(np.e, 1j*phase)], axis=0)[
            np.newaxis, :]
        return np.concatenate([top_row, bot_row], axis=0)

    def reflection_matrix_p(self, theta_incident, previous_layer):
        """
        Returns the reflection matrix used in transfer matrix method when
        given the interface from which light is incident from
        :param previous_layer: Interface with which reflection matrix is
        calculated
        :type previous_layer: Layer
        :param theta_incident: angle at which light is incident to this
        interface
        :type theta_incident: numpy.core.ndarray
        :return: matrix for reflection at the interface.
        :rtype: numpy.core.ndarray
        """
        self._reshape_arrays()
        theta_incident = self._fix_theta(theta_incident)
        previous_layer._reshape_arrays()
        fresnel = SingleInterfaceFresnel()
        fresnel.n_incident = previous_layer.ref_index
        fresnel.n_transmission = self.ref_index
        fresnel.theta_incident = theta_incident
        r = fresnel.amplitude_reflection_p()
        t = fresnel.amplitude_transmission_p()
        top_row = np.concatenate([(1/t)[np.newaxis, :], (r/t)[np.newaxis, :]], axis=0)[np.newaxis, :]
        bot_row = np.concatenate([(r/t)[np.newaxis, :], (1/t)[np.newaxis, :]], axis=0)[np.newaxis, :]
        return np.concatenate([top_row, bot_row], axis=0)

    def _reshape_arrays(self):
        if self.wavelength.size > self.ref_index.size:
            self.ref_index = np.repeat(self.ref_index[0], self.ref_index.size)

    def _fix_theta(self, theta_incident):
        shape = theta_incident.shape
        ret_theta = theta_incident
        if len(shape) < 2:
            ret_theta = np.tile(ret_theta, (self.wavelength.size, 1))
        return ret_theta

    def _fix_wavelength(self, theta_incident):
        theta_shape = theta_incident.shape
        num_angles = theta_shape[1]
        self._calc_wavelength = np.swapaxes(self.wavelength[np.newaxis, :].repeat(num_angles, 0), 0, 1)
        self._calc_index = np.swapaxes(self.ref_index[np.newaxis, :].repeat(num_angles, 0), 0, 1)


if __name__ == "__main__":
    pass
