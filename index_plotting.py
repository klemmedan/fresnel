import matplotlib.pyplot as plt
import os

path = os.path.join("materials", "peald_tin_slowrecipe_onsi.txt")

with open(path, 'r') as f:
    lines = f.readlines()

wavelength = list()
index_n = list()
index_k = list()
for line in lines:
    split_line = line.split()
    w = int(round(float(split_line[0])))
    if w < 1120:
        wavelength.append(w)
        index_n.append(float(split_line[1]))
        index_k.append(float(split_line[2]))

plt.plot(wavelength, index_n, label="Real")
plt.plot(wavelength, index_k, label="Imaginary")

plt.grid(linestyle='--', linewidth=0.5)
plt.xlabel("Wavelength (nm)")
plt.ylabel("TiN Refractive Index")
plt.legend()
plt.savefig(os.path.join("materials", "tin_index_plot.png"), dpi=600)
plt.show()