from TransferMatrixMethod import *
import numpy as np
import matplotlib.pyplot as plt

wavelength = np.array([700])
angle = np.array([0])
layer_thickness = np.linspace(1, 100, num=100)

layer_1 = Layer(np.array([10000]), np.array([1.0]), np.array([wavelength]))
layer_3 = Layer(np.array([10000]), np.array([1.0]), np.array([wavelength]))

tmatrix = MMatrix(wavelength, angle)

results = list()
results_2 = list()
for t in layer_thickness:
    layer_2 = Layer(t, np.array([100.0]), np.array([wavelength]))
    tmatrix.layers = [layer_1, layer_2, layer_3]
    results.append(tmatrix.power_reflection_s()[0][0])
    layer_2 = Layer(t, np.array([.0001]), np.array([wavelength]))

    tmatrix.layers = [layer_1, layer_2, layer_3]
    results_2.append(tmatrix.power_reflection_s()[0][0])

print(results)
plt.plot(layer_thickness, results)
plt.plot(layer_thickness, results_2)
plt.show()